public abstract class Conta {
	
	protected double saldo;
	
	void deposita(double valor) {
		if (valor < 0) {
			throw new ValorInvalidoException(valor);
			}
		else {
			this.saldo += valor - 0.10;
		}
	}

	public double getSaldo() {
		// TODO Auto-generated method stub
		return this.saldo;
	}
	
	public abstract void atualiza(double taxaSelic);
	
}
